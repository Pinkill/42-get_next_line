/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:39:04 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/09 14:58:34 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 50
# endif

# include <stdlib.h>
# include <unistd.h>

typedef struct	s_file
{
	int		fd;
	char	*buf;
	int		res;
	int		chars;
}				t_file;

int				get_next_line(int fd, char **line);
size_t			ft_strlen(const char *str);
char			*ft_strjoin(const char *s1, const char *s2);

size_t			ft_strlen(const char *str);
void			*ft_memcpy(void *dst, const void *src, size_t n);
char			*ft_strndup(const char *src, size_t n);
char			*ft_strjoin(const char *s1, const char *s2);

#endif
