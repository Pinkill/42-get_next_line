# 42 Get Next Line function

A project in 42 study program. It is basically a re-coding of getline function from stdio.h, but with additional control to read buffer size. By setting the "BUFFER_SIZE" macro we can control how many characters, the get next line function, reads from a file at a time.

To see the documentation for the function and how it works, check "en.subject.pdf" file or "man getline".

## Getting Started

When building the project with gcc you can setup the BUFFER_SIZE macro. Example:

```
gcc -D BUFFER_SIZE=100 get_next_line.c -o get_next_line.o
```

This is just a source file without any main function.

Good luck!