/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:36:58 by mknezevi          #+#    #+#             */
/*   Updated: 2020/01/31 19:38:02 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static void	ft_freestorage(char **storage)
{
	if (*storage != 0)
	{
		free(*storage);
		*storage = 0;
	}
}

static int	ft_findstored(char **storage, char **line)
{
	int		i;
	char	*tmp;

	i = 0;
	while ((*storage)[i] != '\0')
	{
		if ((*storage)[i] == '\n')
		{
			if (!(tmp = ft_strndup(*storage + i + 1, BUFFER_SIZE - i - 1)))
				return (-1);
			free(*storage);
			if (!(*storage = ft_strndup(tmp, ft_strlen(tmp))))
				return (-1);
			free(tmp);
			return (0);
		}
		else
		{
			if (!(*line = ft_strjoin(*line, ft_strndup(*storage + i, 1))))
				return (-1);
		}
		i++;
	}
	ft_freestorage(storage);
	return (1);
}

static int	ft_fill(char **line, t_file *file, char **storage)
{
	int	i;
	int	n;

	i = 0;
	n = file->chars;
	while (n-- > 0)
	{
		if (file->buf[i] == '\n')
		{
			if (!(*storage = ft_strndup(file->buf + i + 1,
				BUFFER_SIZE - i - 1)))
				return (-1);
			return (0);
		}
		else
		{
			if (!(*line = ft_strjoin(*line, ft_strndup(file->buf + i, 1))))
				return (-1);
		}
		i++;
	}
	return (1);
}

static int	ft_nextline(char **line, char **storage, t_file *file)
{
	while (file->chars > 0)
	{
		if (*storage == 0)
		{
			if (!(file->buf = malloc(sizeof(char) * (BUFFER_SIZE + 1))))
				return (-1);
			if ((file->chars = read(file->fd, file->buf, BUFFER_SIZE)) < 1)
			{
				free(file->buf);
				return (file->chars);
			}
			file->buf[file->chars] = '\0';
			file->res = ft_fill(line, file, storage);
			free(file->buf);
			if (file->res < 1)
				return (file->res == 0) ? 1 : -1;
		}
		else
		{
			if ((file->res = ft_findstored(storage, line)) < 1)
				return (file->res == 0) ? 1 : -1;
		}
	}
	return (file->res);
}

int			get_next_line(int fd, char **line)
{
	static char	*storage = 0;
	t_file		file;

	file.res = 0;
	file.chars = 1;
	file.buf = 0;
	file.fd = fd;
	if (BUFFER_SIZE < 1 || line == 0)
		return (-1);
	if (!(*line = malloc(sizeof(char) * (1))))
		return (-1);
	*line[0] = '\0';
	file.res = ft_nextline(line, &storage, &file);
	return (file.res);
}
