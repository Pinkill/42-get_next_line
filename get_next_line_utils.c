/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mknezevi <mknezevi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:36:45 by mknezevi          #+#    #+#             */
/*   Updated: 2019/12/07 17:50:11 by mknezevi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

size_t	ft_strlen(const char *str)
{
	size_t i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*ptrdest;
	const char		*ptrsrc;

	ptrdest = dst;
	ptrsrc = src;
	if (dst == 0 && src == 0)
		return (dst);
	while (n-- > 0)
		*ptrdest++ = *ptrsrc++;
	return (dst);
}

char	*ft_strndup(const char *src, size_t n)
{
	char	*dest;
	size_t	i;

	i = ft_strlen(src) + 1;
	if (n < i)
	{
		if (!(dest = (char*)malloc(n + 1)))
			return (0);
		ft_memcpy(dest, src, n);
		dest[n] = '\0';
	}
	else
	{
		if (!(dest = (char*)malloc(i + 1)))
			return (0);
		ft_memcpy(dest, src, i);
		dest[i] = '\0';
	}
	return (dest);
}

char	*ft_strjoin(const char *s1, const char *s2)
{
	char	*dest;
	size_t	sizedest;
	size_t	sizesrc;

	if (s1 == 0 || s2 == 0)
		return (0);
	sizedest = ft_strlen(s1);
	sizesrc = ft_strlen(s2);
	dest = (char*)malloc(sizedest + sizesrc + 1);
	if (!dest)
		return (0);
	ft_memcpy(dest, s1, sizedest);
	ft_memcpy(dest + sizedest, s2, sizesrc);
	dest[sizedest + sizesrc] = '\0';
	free((void *)s2);
	free((void *)s1);
	return (dest);
}
